patch/
├── files
│   ├── 0001-hello-patch-cmake.patch
│   ├── CMakeLists.txt
│   └── hello-patch-cmake.c
├── hello-patch-cmake
│   └── resy
│       └── 0001-hello-patch-cmake.patch
├── hello-patch-cmake-1.0
│   └── resy
│       └── 0001-hello-patch-cmake.patch
├── hello-patch-cmake_1.0.bb
└── readme.txt


$ bitbake hello-patch-cmake-native -e | grep ^FILESPATH=
FILESPATH="
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake-1.0/resy:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake/resy:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/files/resy:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake-1.0/:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake/:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/files/:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake-1.0/x86-64:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake/x86-64:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/files/x86-64:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake-1.0/:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake/:
/workdir/sources/meta-bb-syntax/recipes-patch/patch/files/"


0001-hello-patch-cmake.patch in leftmost (/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake-1.0/resy) wins

if we rename this, 

0001-hello-patch-cmake.patch in leftmost (/workdir/sources/meta-bb-syntax/recipes-patch/patch/hello-patch-cmake/resy) wins

and so on...



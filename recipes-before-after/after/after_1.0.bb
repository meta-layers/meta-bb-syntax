# Copyright (C) 2020 student <student@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

# we want to set DESCIPTION with some string form git repo

#(11:05:28) xavier_deschuyte: OK. By putting logs in a python anonymous function. I understood my issue: each bitbake task trigger a recipe parsing. So I assume my setvar is#working but is wiped by the next recipe parsing
#(11:05:39) yann|work [~yann@91-170-159-152.subs.proxad.net] entered the room.
#(11:07:21) paulbarker: xavier_deschuyte: Each task has it's own context. Are you trying to set a value in one task and then get it in another task?

#I used that: `do_myfunc[postfuncs] += "do_myfunc:append"` that did the trick and context is preserved but would be interrested to fix it with append/prepend

#############

# With a given task, variable changes are only local. This means do_unpack does not 'see' a change made by the do_fetch task.

#This is necessary to allow some tasks to rerun when others are covered by sstate, to ensure things are deterministic.

#If you really want to do what you describe, you'd need something like a prefunc for the tasks where you need to modify SRC_URI.
#
#python myprefunc() {
#    d.appendVar("SRC_URI", "https://www.bla.com/resource.tar")
#}
#do_fetch[prefuncs] += "myprefunc"
#do_unpack[prefuncs] += "myprefunc"
#However note that whilst this will do some of what you want, source archives, license manifests and sstate checksums may not work correctly since you're "hiding" source data from bitbake and this data is only present at task execution time, not parse time.

###################

# those variables are local
# in .conf files we have global variables

SUMMARY = "a"
DESCRIPTION = "b"
LICENSE = "CLOSED"
MYVAR = "c"

python do_myfunc() {
    bb.warn("myfunc start")
    bb.warn("before SUMMARY=%s" % (d.getVar("SUMMARY")))
    bb.warn("before DESCRIPTION=%s" % (d.getVar("DESCRIPTION")))
    bb.warn("before MYVAR=%s" % (d.getVar("MYVAR")))
    d.setVar("SUMMARY", "pouet")
    d.setVar("DESCRIPTION", "pouet")
    d.setVar("MYVAR", "pouet")
    bb.warn("after SUMMARY=%s" % (d.getVar("SUMMARY")))
    bb.warn("after DESCRIPTION=%s" % (d.getVar("DESCRIPTION")))
    bb.warn("after MYVAR=%s" % (d.getVar("MYVAR")))
    bb.warn("myfunc end")
} 

python do_myfunc2() {
    bb.warn("myfunc2 start")
    bb.warn("SUMMARY=%s" % (d.getVar("SUMMARY")))
    bb.warn("DESCRIPTION=%s" % (d.getVar("DESCRIPTION")))
    bb.warn("MYVAR=%s" % (d.getVar("MYVAR")))
    bb.warn("myfunc2 end")
}

python do_myfunc3() {
    bb.warn("myfunc3 start")
    DESCRIPTION="lumpi"
    d.setVar("DESCRIPTION", "lumpi")
    bb.warn("myfunc3 end")
}

python myprefunc() {
    d.setVar("MYVAR", "https://www.bla.com/resource.tar")
}
do_myfunc[prefuncs] += "myprefunc"
do_myfunc2[prefuncs] += "myprefunc"

addtask myfunc after do_unpack before do_build
addtask myfunc2 after do_myfunc before do_build
#addtask myfunc3 after do_myfunc2 before do_build

do_myfunc2[prefuncs] += "do_myfunc3"



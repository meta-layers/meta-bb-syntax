1) add layer to bblayers.conf

/workdir/sources/meta-bb-syntax

2)

bitbake immediate -c cleanall

2.1)

# -->
TOPIMMEDIATE := "${THISDIR}/lumpi:"

do_immediate_top() {
    bbwarn "FILESEXTRAPATHS: ${FILESEXTRAPATHS}"
    bbwarn "TOPIMMEDIATE: ${TOPIMMEDIATE}"
}
addtask do_immediate_top
# do_immediate_top: FILESEXTRAPATHS: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:__default:
# do_immediate_top: TOPIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:
# <--

bitbake immediate -c do_immediate_top

WARNING: immediate-1.0-r0 do_immediate_top: FILESEXTRAPATHS: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:__default:
WARNING: immediate-1.0-r0 do_immediate_top: TOPIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:

2.2)

# -->
TOPLAZY = "${THISDIR}/lumpi:"

do_lazy_top() {
    bbwarn "TOPLAZY: ${TOPLAZY}"
}
addtask do_lazy_top
# do_lazy_top: TOPLAZY: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:
# <--

bitbake immediate -c do_lazy_top

WARNING: immediate-1.0-r0 do_lazy_top:      TOPLAZY:         /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:

2.3)

require lumpi/immediate.inc

...

# -->
INCIMMEDIATE := "${THISDIR}/lumpine:"

do_immediate_inc() {
    bbwarn "INCIMMEDIATE: ${INCIMMEDIATE}"
}
addtask do_immediate_inc
# do_immediate_inc: INCIMMEDIATE: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi/lumpine:
# <--

bitbake immediate -c do_immediate_inc

WARNING: immediate-1.0-r0 do_immediate_inc: INCIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi/lumpine:

2.4)

require lumpi/immediate.inc

...

# -->
INCLAZY = "${THISDIR}/lumpine:"

do_lazy_inc() {
    bbwarn "INCLAZY: ${INCLAZY}"
}
addtask do_lazy_inc
# do_lazy_inc: INCLAZY: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpine:
# <--

bitbake immediate -c do_lazy_inc

WARNING: immediate-1.0-r0 do_lazy_inc:      INCLAZY:         /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpine:




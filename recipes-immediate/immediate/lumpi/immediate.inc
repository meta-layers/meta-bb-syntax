# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

# -->
INCIMMEDIATE := "${THISDIR}/lumpine:"

do_immediate_inc() {
    bbwarn "INCIMMEDIATE: ${INCIMMEDIATE}"
}
addtask do_immediate_inc
# do_immediate_inc: INCIMMEDIATE: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi/lumpine:
# <--

# -->
INCLAZY = "${THISDIR}/lumpine:"

do_lazy_inc() {
    bbwarn "INCLAZY: ${INCLAZY}"
}
addtask do_lazy_inc
# do_lazy_inc: INCLAZY: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpine:
# <--

# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "immediate operator"
DESCRIPTION = "immediate operator"
LICENSE = "CLOSED"

FILESEXTRAPATHS:prepend := "${THISDIR}/lumpi:"

require lumpi/immediate.inc

# -->
TOPIMMEDIATE := "${THISDIR}/lumpi:"

do_immediate_top() {
    bbwarn "FILESEXTRAPATHS: ${FILESEXTRAPATHS}"
    bbwarn "TOPIMMEDIATE: ${TOPIMMEDIATE}"
}
addtask do_immediate_top
# do_immediate_top: FILESEXTRAPATHS: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:__default:
# do_immediate_top: TOPIMMEDIATE:    /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:
# <--

# -->
TOPLAZY = "${THISDIR}/lumpi:"

do_lazy_top() {
    bbwarn "TOPLAZY: ${TOPLAZY}"
}
addtask do_lazy_top
# do_lazy_top: TOPLAZY: /workdir/sources/meta-bb-syntax/recipes-immediate/immediate/lumpi:
# <--

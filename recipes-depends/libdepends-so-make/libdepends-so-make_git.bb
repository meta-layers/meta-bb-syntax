# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "libdepends.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/libdepends-makefile-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.2.3+git${SRCPV}"

inherit runtime-depends

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass those to my Makefile
EXTRA_OEMAKE =+ "includedir=${D}${includedir} libdir=${D}${libdir}"

# all the work is done in the Makefile
# just creating dirs and calling make install
do_install () {
    # create them in case they are not there
    install -d ${D}${includedir}
    install -d ${D}${libdir}
    # install
    oe_runmake install
}

# just as a precaution for crappy Makefiles and multi-core
PARALLEL_MAKE=""

# Something like this is what we want:

# oe-pkgdata-util list-pkg-files libdepends-so-make
# libdepends-so-make:
#        /usr/lib/libdepends.so.1

# oe-pkgdata-util list-pkg-files libdepends-so-make-dev
# libdepends-so-make-dev:
#        /usr/include/depends.h
#        /usr/lib/libdepends.so

# + bitbake -s | grep libdepends
# libdepends-so-make                  :1.2.3+gitAUTOINC+db7569e190-r0                          
# use-libdepends-makefile-so          :1.0+gitAUTOINC+f2fe4692b2-r0  

# + bitbake libdepends-so-make -e | grep ^DEPENDS
# DEPENDS="virtual/arm-resy-linux-gnueabi-gcc virtual/arm-resy-linux-gnueabi-compilerlibs virtual/libc "

# + bitbake libdepends-so-make -e | grep ^RDEPENDS
# RDEPENDS=""
# RDEPENDS:libdepends-so-make-dev="libdepends-so-make (= 1.2.3+gitAUTOINC+db7569e190-r0)"
# RDEPENDS:libdepends-so-make-staticdev="libdepends-so-make-dev (= 1.2.3+gitAUTOINC+db7569e190-r0)"

# + bitbake libdepends-so-make -e | grep ^PACKAGES
# PACKAGES="libdepends-so-make-src libdepends-so-make-dbg libdepends-so-make-staticdev libdepends-so-make-dev libdepends-so-make-doc libdepends-so-make-locale  libdepends-so-make"
# PACKAGESPLITFUNCS="                 package_do_split_locales                 populate_packages"
# PACKAGES_DYNAMIC="^libdepends-so-make-locale-.*"

# without the additional PROVIDES the default PROVIDES list:
# + bitbake libdepends-so-make -e | grep ^PROVIDES
# PROVIDES="libdepends-so-make "

# PROVIDES: A list of aliases by which a particular recipe can be known.
PROVIDES += "virtual/libdepends"

# after adding a new item to the PROVIDES list:
# + bitbake libdepends-so-make -e | grep ^PROVIDES
# PROVIDES="libdepends-so-make  virtual/libdepends"

# RPROVIDES: A list of package name aliases that a package also provides.
RPROVIDES:${PN} = "${PN}-pkg"

# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "use libdepends.so.1.2.3 with makefile"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://gitlab.com/exempli-gratia/use-libdepends-makefile-so.git;protocol=https;branch=master" 
SRCREV = "${AUTOREV}"
PV = "1.0+git${SRCPV}"

#inherit runtime-depends

# with this .h and .so files plus symlinks
# should be at the proper places to be found
# DEPENDS: build time dependencies (recipe name)
# DEPENDS = "libdepends-so-make"

# the libdepends-so-make recipe also PROVIDES virtual/libdepends
DEPENDS = "virtual/libdepends"

# this is just for the package manager
# so libfoo-so-make is installed automatically
# if you install this testcase
# RDEPENDS: run time dependencies (package name)
# the libdepends-so-make recipe also RPROVIDES-${PN} libdepends-so-make-pkg
RDEPENDS:${PN} = "libdepends-so-make-pkg (>= 1.2)"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

# I want to pass this to my Makefile
EXTRA_OEMAKE = "DESTDIR=${D}${bindir} SOMEFILEDESTDIR=${D}/somedir"

# all the work is done in the Makefile
# just creating dir and calling make install
do_install() {
        # create the dir if it does not already exist
        install -d ${D}${bindir}
        # create the dir if it does not already exist
        install -d ${D}/somedir
        # install test case in there
        oe_runmake install
}

# just as a precaution against multi-core and crappy Makefiles
PARALLEL_MAKE=""

# Something like this is what we want:
# + oe-pkgdata-util list-pkgs use-libdepends*
# use-libdepends-makefile-so
# use-libdepends-makefile-so-dbg
# use-libdepends-makefile-so-dev

# + oe-pkgdata-util list-pkg-files use-libdepends-makefile-so
# use-libdepends-makefile-so:
#        /usr/bin/use-libdepends-makefile-so

# + oe-pkgdata-util list-pkg-files use-libdepends-makefile-so-dbg
# use-libdepends-makefile-so-dbg:
#         /usr/lib/debug/usr/bin/use-libdepends-makefile-so.debug
#         /usr/src/debug/use-libdepends-makefile-so/1.2.3+gitAUTOINC+f2fe4692b2-r0/git/use-libdepends-makefile-so.c

# + oe-pkgdata-util list-pkg-files use-libdepends-makefile-so-dev
# use-libdepends-makefile-so-dev:

# + bitbake -s | grep libdepends
# libdepends-so-make                  :1.2.3+gitAUTOINC+db7569e190-r0                          
# use-libdepends-makefile-so          :1.0+gitAUTOINC+f2fe4692b2-r0  

# bitbake libdepends-so-make -e | grep ^PROVIDES
# PROVIDES="libdepends-so-make "

# ERROR: use-libdepends-makefile-so-1.0+gitAUTOINC+1fc06f22ea-r0 do_package: QA Issue: use-libdepends-makefile-so: Files/directories were installed but not shipped in any package:
#  /somedir
#  /somedir/some-file.txt
# Please set FILES such that these items are packaged. Alternatively if they are unneeded, avoid installing them or delete them within do_install.
# use-libdepends-makefile-so: 2 installed and not shipped files. [installed-vs-shipped]

PACKAGES =+ "${PN}-somepkg"

FILES:${PN}-somepkg += "somedir/some-file.txt"

# $ oe-pkgdata-util list-pkgs use-libdepends-makefile-so*
# use-libdepends-makefile-so
# use-libdepends-makefile-so-dbg
# use-libdepends-makefile-so-dev
# use-libdepends-makefile-so-somepkg

# $ oe-pkgdata-util list-pkg-files use-libdepends-makefile-so
# use-libdepends-makefile-so:
#        /usr/bin/use-libdepends-makefile-so

# $ oe-pkgdata-util list-pkg-files use-libdepends-makefile-so-dbg
# use-libdepends-makefile-so-dbg:
#         /usr/lib/debug/usr/bin/use-libdepends-makefile-so.debug
#         /usr/src/debug/use-libdepends-makefile-so/1.0+gitAUTOINC+1fc06f22ea-r0/git/use-libdepends-makefile-so.c

# $ oe-pkgdata-util list-pkg-files use-libdepends-makefile-so-dev
# use-libdepends-makefile-so-dev:

# $ oe-pkgdata-util list-pkg-files use-libdepends-makefile-so-somepkg
# use-libdepends-makefile-so-somepkg:
#         /somedir/some-file.txt

# $ bitbake use-libdepends-makefile-so -e | grep ^DEPENDS
# DEPENDS="virtual/arm-resy-linux-gnueabi-gcc virtual/arm-resy-linux-gnueabi-compilerlibs virtual/libc virtual/libdepends"

# $ bitbake use-libdepends-makefile-so -e | grep ^RDEPENDS
# RDEPENDS=""
# RDEPENDS:use-libdepends-makefile-so="libdepends-so-make (>= 1.2)"
# RDEPENDS:use-libdepends-makefile-so-dev="use-libdepends-makefile-so (= 1.0+gitAUTOINC+1fc06f22ea-r0)"
# RDEPENDS:use-libdepends-makefile-so-staticdev="use-libdepends-makefile-so-dev (= 1.0+gitAUTOINC+1fc06f22ea-r0)"

# $ bitbake use-libdepends-makefile-so -e | grep ^PACKAGES
# PACKAGES="use-libdepends-makefile-so-somepkg use-libdepends-makefile-so-src use-libdepends-makefile-so-dbg use-libdepends-makefile-so-staticdev use-libdepends-makefile-so-dev use-libdepends-makefile-so-doc use-libdepends-makefile-so-locale  use-libdepends-makefile-so"
# PACKAGESPLITFUNCS="                 package_do_split_locales                 populate_packages"
# PACKAGES_DYNAMIC="^use-libdepends-makefile-so-locale-.*"


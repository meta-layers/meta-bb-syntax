https://docs.yoctoproject.org/bitbake/bitbake-user-manual/bitbake-user-manual-ref-variables.html#term-DEPENDS

https://docs.yoctoproject.org/bitbake/bitbake-user-manual/bitbake-user-manual-ref-variables.html#term-PROVIDES


https://docs.yoctoproject.org/bitbake/bitbake-user-manual/bitbake-user-manual-ref-variables.html#term-RDEPENDS

https://docs.yoctoproject.org/bitbake/bitbake-user-manual/bitbake-user-manual-ref-variables.html#term-RPROVIDES


https://docs.yoctoproject.org/singleindex.html#automatically-added-runtime-dependencies

-->

https://docs.yoctoproject.org/singleindex.html#term-PRIVATE_LIBS

Specifies libraries installed within a recipe that should be ignored by the OpenEmbedded build system’s shared library resolver. This variable is typically used when software being built by a recipe has its own private versions of a library normally provided by another recipe. In this case, you would not want the package containing the private libraries to be set as a dependency on other unrelated packages that should instead depend on the package providing the standard version of the library.


./meta-openembedded/meta-oe/recipes-extended/collectd/collectd_5.12.0.bb:

# threshold.so load.so are also provided by gegl
# disk.so is also provided by libgphoto2-camlibs
PRIVATE_LIBS = "threshold.so load.so disk.so"

<--

meta/recipes-core/dbus/dbus-test_1.12.16.bb
PRIVATE_LIBS:${PN}-ptest = "libdbus-1.so.3"

meta/recipes-devtools/elfutils/elfutils_0.178.bb:
PRIVATE_LIBS:${PN}-ptest = "libdw.so.1 libelf.so.1 libasm.so.1"

https://docs.yoctoproject.org/singleindex.html#term-RCONFLICTS

The list of packages that conflict with packages.

# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "call shell task from python task"
DESCRIPTION = "shell from python"
LICENSE = "CLOSED"

##################################################################
# How can we call a shell function from a python function?
#
# The common practice is to add an intermediate python function 
# that is calling the shell function.
#
# Using bb.build.exec_func and bb.build.exec_task you can call the  
# functions/tasks you want.
##################################################################

# first Python func
python do_myfunc() {
    bb.warn("do_myfunc start")
    bb.warn("do_myfunc end")
} 

# intermediate to call after/before do_myfunc
# should call do_myshell
python do_myfunc2() {
    bb.warn("do_myfunc2 start")
    bb.build.exec_func('do_myshell', d)
    bb.warn("do_myfunc2 end")
}

# do my_shell
do_myshell() {
   bbwarn "shell func start"
   bbwarn "shell func end"
}

#python do_myfunc2() {
#    bb.warn("myfunc2 start")
#    bb.warn("SUMMARY=%s" % (d.getVar("SUMMARY")))
#    bb.warn("DESCRIPTION=%s" % (d.getVar("DESCRIPTION")))
#    bb.warn("MYVAR=%s" % (d.getVar("MYVAR")))
#    bb.warn("myfunc2 end")
#}

#python do_myfunc3() {
#    bb.warn("myfunc3 start")
#    DESCRIPTION="lumpi"
#    d.setVar("DESCRIPTION", "lumpi")
#    bb.warn("myfunc3 end")
#}

#addtask myfunc after do_unpack before do_build
#addtask myfunc2 after do_myfunc before do_build
#addtask myfunc3 after do_myfunc2 before do_build

#do_myfunc2[prefuncs] += "do_myfunc3"

addtask do_myfunc
addtask do_myfunc2
addtask do_myshell

do_myfunc[postfuncs] += "do_myfunc2"

# Copyright (C) 2017 genius user <genius@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Simple helloworld cmake"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://CMakeLists.txt \
           file://hello-bbappend-one-cmake.c"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

inherit cmake

EXTRA_OECMAKE = ""

BBCLASSEXTEND="native"


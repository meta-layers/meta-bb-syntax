DESCRIPTION = "Simple helloworld cpp cmake application"
SECTION = "examples"
LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=c79ff39f19dfec6d293b95dea7b07891"

SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/exempli-gratia/hellocppcmake.git;protocol=https;branch=alt2"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# eclipse-cmake-git_0.0+git4+ecf1f0bc87-r0.0:armv7a-vfp-neon.ipk
PV = "1.0+git${SRCPV}"
S = "${WORKDIR}/git"

inherit cmake update-alternatives

# This is supposed to be selected as default:
ALTERNATIVE_PRIORITY = "11"

ALTERNATIVE:${PN} = "hellocppcmake"
ALTERNATIVE_TARGET[hellocppcmake] = "${bindir}/hellocppcmake"

RPROVIDES:${PN} += "hellocppcmake"

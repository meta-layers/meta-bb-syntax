DESCRIPTION = "Simple helloworld cpp cmake application"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "${AUTOREV}"
SRC_URI = "git://gitlab.com/exempli-gratia/hellocppcmake.git;protocol=https;branch=alt1"
# Note: this only works with PR_server running!
# PV is expanded to something like 0.0+gitAUTOINC+ecf1f0bc87"
# The PR_server? replaces AUTOINC with a number in the package name
# eclipse-cmake-git_0.0+git4+ecf1f0bc87-r0.0:armv7a-vfp-neon.ipk
PV = "1.0+git${SRCPV}"
S = "${WORKDIR}/git"

inherit cmake update-alternatives

ALTERNATIVE_PRIORITY = "10"
ALTERNATIVE:${PN} = "hellocppcmake"
ALTERNATIVE_TARGET[hellocppcmake] = "${bindir}/hellocppcmake"

RPROVIDES:${PN} += "hellocppcmake"

LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESPATH =. "${FILE_DIRNAME}/${BPN}-7:"

SRC_URI = "file://lumpi-7.txt"

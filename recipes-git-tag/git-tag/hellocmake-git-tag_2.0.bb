# Copyright (C) 2023 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "git with tag"
SECTION = "examples"
SUMMARY = "git with tag"
HOMEPAGE = "https:/www.ReliableEmbeddedSystems.com"
RECIPE_MAINTAINER = "Robert Berger <Robert.Berger@ReliableEmbeddedSystems.com>"
LICENSE = "MIT"
SECTION = "examples"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

# -->
# don't use tag, since like this an offline build won't work
# https://wiki.yoctoproject.org/wiki/How_do_I#Q:_How_do_I_create_my_own_source_download_mirror_?
#
# If you are building with BB_NO_NETWORK and using external layers that are not part of
# the base Yocto distribution, be aware that recipes which:
#
# Specify a Git repo as the source, and,
# Specify the revision to be built using a tag name
# will cause your build to abort when Bitbake tries to run git ls-remote to resolve the tag 
# name and is unable to access the network. So don't specify SRC_URI like this:
# 
# SRC_URI = "git://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git;protocol=git;tag=v${PV}"
#
# Instead, use:
# 
# SRC_URI = "git://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git;protocol=git"
# SRCREV = "8885ced062131214448fae056ef453f094303805"
#
# Before putting together your pre-mirror, this command can be used from your top-level 
# source directory to identify recipes that might be problematic with BB_NO_NETWORK enabled:
#
# find -name "*.inc" -o -name "*.bb" | xargs grep -li "git;tag="
# <--

TAG:="${PV}"

SRC_URI = "git://gitlab.com/exempli-gratia/hellocmake-git-tag.git;protocol=https;branch=master;tag=v${TAG}"
PV = "${TAG}+git${SRCPV}"

# sources are under ${WORKDIR}/git <--
S = "${WORKDIR}/git"

DEFAULT_PREFERENCE = "-1"
